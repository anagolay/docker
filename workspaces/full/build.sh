#!/usr/bin/env bash
set -x
set -o errexit

root="$(git rev-parse --show-toplevel)"

full_path=$(realpath $0)
dir_path=$(dirname $full_path)

VARIANT=${1:-"bullseye"}
NODE_VERSION=${2:-"16"}
RUST_TOOLCHAIN=${3:-"stable-2022-01-20"}

IMAGE_NAME="workspace"
IMAGE_VERSION="full_devcontainer_node${NODE_VERSION}_rust${RUST_TOOLCHAIN}_${VARIANT}"

docker build \
	--build-arg VARIANT=${VARIANT} \
	--build-arg NODE_VERSION=${NODE_VERSION} \
	--build-arg RUST_TOOLCHAIN=${RUST_TOOLCHAIN} \
	--tag anagolay/${IMAGE_NAME}:${IMAGE_VERSION} \
	--file ./devcontainer/full-workspace/Dockerfile \
	.

docker push docker.io/anagolay/${IMAGE_NAME}:${IMAGE_VERSION}
