#!/usr/bin/env bash
set -x

# ROOT=$(git rev-parse --show-toplevel)

full_path=$(realpath $0)
dir_path=$(dirname $full_path)

VARIANT=${1:-"bullseye"}
RUST_TOOLCHAIN=${2:-"stable-x86_64-unknown-linux-gnu"}

docker build \
	--build-arg VARIANT=${VARIANT} \
	--build-arg RUST_TOOLCHAIN=${RUST_TOOLCHAIN} \
	--build-arg RUST_MAIN_VERSION=${RUST_TOOLCHAIN} \
	--tag anagolay/rust:1-${RUST_TOOLCHAIN}-${VARIANT} \
	--file $dir_path/Dockerfile \
	.

if [[ "${1}" =~ "push" ]]; then
	docker push docker.io/anagolay/rust:1-${RUST_TOOLCHAIN}-${VARIANT}
fi
